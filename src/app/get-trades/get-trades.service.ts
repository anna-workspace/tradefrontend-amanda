import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Trade } from '../model/trade';
import { URLs } from '../../environments/environment';
import { AuthenticationService } from '../authentication/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class GetTradesService {

  private readonly url : string = URLs.tradeService + '/getHistory';

  constructor(private httpClient: HttpClient, private authentication : AuthenticationService) { }

  getHistory() : Observable<Array<Trade>> {
    let history = this.httpClient.get<Array<Trade>>(
      this.url, {headers: this.authentication.authorisation, responseType: 'json'}
    );
    return history;
  }

  getHistoryByTicker(ticker : string) : Observable<Array<Trade>>{
    let history = this.httpClient.get<Array<Trade>> (
      this.url + '?' + ticker, {headers: this.authentication.authorisation, responseType: 'json'}
    );
    return history;
  }

}
