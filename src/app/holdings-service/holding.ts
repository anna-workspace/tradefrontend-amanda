
export class Holding {

    public balance : number;
    
    constructor(public ticker : string, public companyName : string, public bought : number, public sold : number) {}

}
