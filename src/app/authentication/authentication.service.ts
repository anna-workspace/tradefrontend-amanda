import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { URLs } from '../../environments/environment';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  public authorisation : HttpHeaders = new HttpHeaders({
    'x-api-key': '11052EF4C22244C78D35DFAA7EDDEFE5'
  });

  private readonly url : string = URLs.userService;

  private readonly login : string = '/login';

  constructor(private http : HttpClient) { }

  public authenticate(credential : Credential) : boolean {
    this.http.post(
      this.url + this.login, credential, 
      {observe: 'response', responseType: 'text'}
    ).subscribe(response => {
      if (response.ok) {
        this.constructApiHeader(response.body);
        return true;
      }
    });
    return false;
  }

  private constructApiHeader(apiKey : string) : void {
    this.authorisation = new HttpHeaders({
      'x-api-key': apiKey
    });
  }

}
