import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {URLs} from '../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TradeService {

  private url = URLs.tradeService + URLs.createTradeService;
  constructor(private httpClient: HttpClient) {}

  createTrade(trade: Object): Observable<Object>{
    return this.httpClient.post(`${this.url}`, trade);
  }
}



